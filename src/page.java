import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
//用户页面
public class page extends JFrame {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/library?" +
            "useSSL=false & allowPublicKeyRetrieval=true & serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";
    Connection conn = null;
    PreparedStatement stmt = null,stmt2 = null;
    ResultSet rs = null,rs2 =null;
    JPanel panel1;
    JMenuBar menuBar;
    JMenu userInf, recommend;
    JMenuItem inf,recommendBook,change;
    JTextArea infArea;

    ActionListener infAct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("个人信息")) {
                infArea.setText("");
                setInformation();
            }
        }
    };
    ActionListener changeAct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("修改信息")) {
                changePage page = new changePage();
                page.setVisible(true);
            }
        }
    };
    ActionListener reAct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("推荐图书")) {
                infArea.setText("");
                infArea.append("\t\t推荐图书：\n");
                String sign = new dataRoom().select_sign();
                test_re(dataRoom.personPower,sign);
            }
        }
    };
    public page(){
        setTitle("用户中心");
        setSize(800,500);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setPage();
    }
    public void setPage(){
        new dataRoom().getPerson(Login.account);
        panel1 = new JPanel();
        menuBar = new JMenuBar();
        infArea = new JTextArea(20,110);
        Font font = new Font("宋体",Font.PLAIN,20);
        infArea.setFont(font);
        panel1.add(infArea);
        recommend = new JMenu("推荐");
        recommendBook = new JMenuItem("推荐图书");
        recommendBook.addActionListener(reAct);
        recommend.add(recommendBook);
        userInf = new JMenu("权限");
        inf = new JMenuItem("个人信息");
        inf.addActionListener(infAct);
        change = new JMenuItem("修改信息");
        change.addActionListener(changeAct);
        menuBar.add(userInf);
        menuBar.add(recommend);
        userInf.add(change);
        add(panel1, BorderLayout.CENTER);
        userInf.add(inf);
        setJMenuBar(menuBar);
    }
    public void setInformation(){
        infArea.append("");
        infArea.append("\t\t用户信息：\n\t用户ID\t用户账号\t\t用户名\t专业\t爱好");
        infArea.append("\n\t"+dataRoom.userid+"\t"+Login.account+"\t"+dataRoom.userName+"\t"+dataRoom.personPower+"\t"+dataRoom.personLike);
    }
    public void test_re(String power,String sign){
        try{
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            int n = 1;
            // 打开链接
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            // 执行查询
            String sql1 = null;
            String sql2 = null;
            switch (n){
                case 1:
                    infArea.append("\t\t按专业推荐：\n");
                    infArea.append("\t\t图书编号\t\t图书名称\t\t作者\t\t标签"+"\n");
                    sql2 = "select ISBN, bName, aName, sign from book where sign = ?";
                    stmt2= conn.prepareStatement(sql2);
                    stmt2.setString(1, power);
                    rs2 = stmt2.executeQuery();
                    while(rs2.next()){
                        if(!new dataRoom().judge_recommend(rs2.getString("bName"),dataRoom.userid) ) {
                            infArea.append("\t\t"+rs2.getString("ISBN") + "\t\t" + rs2.getString("bName")
                                    + "\t\t" + rs2.getString("aName") + "\t\t" + rs2.getString("sign") + "\n");
                        }
                    }
                case 2:
                    infArea.append("\t\t按标签推荐：\n");
                    infArea.append("\t\t图书编号\t\t图书名称\t\t作者\t\t标签" + "\n");
                    sql1 = "select ISBN, bName, aName, sign from book where sign = ?";
                    stmt = conn.prepareStatement(sql1);
                    stmt.setString(1, sign);
                    rs = stmt.executeQuery();
                    while(rs.next()){
                        if(!new dataRoom().judge_recommend(rs.getString("bName"),dataRoom.userid) ) {
                            infArea.append("\t\t"+rs.getString("ISBN") + "\t\t" + rs.getString("bName")
                                    + "\t\t" + rs.getString("aName") + "\t\t" + rs.getString("sign") + "\n");
                        }
                    }
            }
            // 完成后关闭
            rs.close();
            rs2.close();
            stmt.close();
            stmt2.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            // 关闭资源
            try{
                if(stmt!=null) stmt.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}

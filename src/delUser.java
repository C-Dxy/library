import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class delUser extends JFrame{
    //删除用户页面
        JPanel panel;
        Label IDLabel;
        JTextField IDFiled;
        JButton del;
        boolean chooce;
        ActionListener dAL=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals("删除")) {
                    int ID = Integer.parseInt(IDFiled.getText());
                    String isbn = IDFiled.getText();
                    boolean sign = true;
                    if(chooce)
                        new dataRoom().delete(ID,sign);
                    else
                        new dataRoom().delBook(isbn);
                    dispose();
                }
            }
        };
        public delUser(boolean choice) {
            chooce = choice;
            if(chooce) {
                setTitle("删除用户");
                IDLabel = new Label("用户ID");
            }
            else {
                setTitle("删除图书");
                IDLabel = new Label("图书ISBN");
            }
            setSize(300, 200);
            setLocationRelativeTo(null);
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            panel=new JPanel();
            panel.setLayout(new GridLayout(2, 2));
            del = new JButton("删除");
            del.addActionListener(dAL);
            IDFiled=new JTextField();
            panel.add(IDLabel);
            panel.add(IDFiled);
            panel.add(del);
            add(panel,BorderLayout.CENTER);
        }
}

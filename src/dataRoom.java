import java.sql.*;
//数据库函数
public class dataRoom {
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/library?" +
            "useSSL=false & allowPublicKeyRetrieval=true & serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";
    Connection conn = null;
    PreparedStatement stmt = null,stmt2 = null,stmt3 = null,stmt4 = null;
    Statement pst;
    ResultSet rs = null;
    public static String BookIsbn, BookName, Author, Sign;
    public static String personPower,personLike;
    public static int userid;
    public static String userName;

    public static int BookNumber;//书籍数量
    public static int limitNum;

    public void getLimitNum(int currentId) {
        /*计算当前登录账号已借的图书数量
        * 超过三本无法再借
        * */
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql;
            sql = "select count(id) from browinformation where id = ?";//sql语句
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, currentId);
            rs = stmt.executeQuery();
            // 展开结果集数据库
            while (rs.next()) {
                // 通过字段检索
                limitNum = rs.getInt("count(Id)");
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public boolean judge_recommend(String judgeName, int userAccount) {
        /*用于判断用户是否曾经借阅过该书，借阅过则不推荐*/
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql;
            sql = "select browBook from browrecord where browBook = ? and browId =?";//sql语句
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, judgeName);
            stmt.setInt(2, userAccount);
            rs = stmt.executeQuery();
            // 展开结果集数据库
            while (rs.next()) {
                String name = rs.getString("browBook");
                // 通过字段检索
                return true;
            }
            // 完成后关闭
            rs.close();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return false;
    }
    public boolean judge_book(String judgeName, int userAccount) {
        /*判断用户的书架是否存在该书，存在则无法借阅*/
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql;
            sql = "select bookName from browinformation where bookName = ? and id =?";//sql语句
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, judgeName);
            stmt.setInt(2, userAccount);
            rs = stmt.executeQuery();
            // 展开结果集数据库
            while (rs.next()) {
                String name = rs.getString("bookName");
                // 通过字段检索
                return true;
            }
            // 完成后关闭
            rs.close();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return false;
    }

    public void setInformation(String a, String b, String c, String d, int e) {
        /*获取图书信息，用于搜索功能返回完整的图书信息*/
        BookIsbn = a;
        BookName = b;
        Author = c;
        Sign = d;
        BookNumber = e;
    }
    public void getPerson(String a) {
        /*获取当前登录账号的个人信息*/
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql;
            sql = "SELECT userid,name,power,love FROM people where account = ?";//sql语句
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, a);
            rs = stmt.executeQuery();
            // 展开结果集数据库
            while (rs.next()) {
                // 通过字段检索
                userid = rs.getInt("userid");
                personPower = rs.getString("power");
                personLike = rs.getString("love");
                userName = rs.getString("name");
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    //超级管理员权限
    public void delMana(int id) {
        /*删除管理员，用于超级管理员*/
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            String sql = "delete from people where userid = ?";
            String sql2 =  "delete from post where emId = ?";
            stmt= conn.prepareStatement(sql);
            stmt2 = conn.prepareStatement(sql2);
            stmt.setInt(1,id);
            stmt2.setInt(1,id);
            stmt.executeUpdate();
            stmt2.executeUpdate();
        }catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null) stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
    public void insertMana(int id,String account, String name, String password) {
        /*向数据库中插入管理员信息，用于超级管理员*/
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql = "insert into post(emId,emAccount,emName,emPassword) values (?,?,?,?)";
            String sql2 = "insert into people(userid,account,name,password) values (?,?,?,?)";
            stmt = conn.prepareStatement(sql);
            stmt2 = conn.prepareStatement(sql2);
            //向数据库插入数据
            stmt.setInt(1, id);
            stmt.setString(2, account);
            stmt.setString(3, name);
            stmt.setString(4, password);
            stmt2.setInt(1, id);
            stmt2.setString(2, account);
            stmt2.setString(3, name);
            stmt2.setString(4, password);
            stmt.executeUpdate();
            stmt2.executeUpdate();
            // 完成后关闭
            stmt.close();
            stmt2.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    //管理员权限
    public void delRecode(int id,String name){
        //删除用户关于某本书的借阅记录
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql = "delete from browrecord where browId = ? and browBook = ?";
            String sql2 = "delete from browreturn where returnId = ? and returnBook = ?";
            stmt = conn.prepareStatement(sql);
            stmt2 = conn.prepareStatement(sql2);
            //向数据库插入数据
            stmt.setInt(1, id);
            stmt.setString(2, name);
            stmt.executeUpdate();
            stmt2.setInt(1, id);
            stmt2.setString(2, name);
            stmt2.executeUpdate();
            // 完成后关闭
            stmt.close();
            stmt2.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    public void insertAuthor(String name, String nation) {
        /*向数据库中插入作者信息*/
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql2 = "insert into author(aName, nation) values (?,?)";
            stmt = conn.prepareStatement(sql2);
            //向数据库插入数据
            stmt.setString(1, name);
            stmt.setString(2, nation);
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    public void delBook(String isbn){
        //删除图书
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
             String  sql = "delete from book where ISBN = ?";
            stmt = conn.prepareStatement(sql);
            //向数据库插入数据
            stmt.setString(1, isbn);
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    public void insertBook(String ISBN, String bName, String aName, String sign, int number,boolean choice) {
        /*向数据库中插入图书信息，用于上传资源*/
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql = null;
            // 执行查询
            if(choice) {
                sql = "insert into book(ISBN,bName,aName,sign,number) values (?,?,?,?,?)";
            }
            else {
                sql = "update book set ISBN = ?,bName = ?,aName = ?,sign = ?,number = ? where ISBN = ?";
            }
            stmt = conn.prepareStatement(sql);
            //向数据库插入数据
            stmt.setString(1, ISBN);
            stmt.setString(2, bName);
            stmt.setString(3, aName);
            stmt.setString(4, sign);
            stmt.setInt(5, number);
            if(! choice)
                stmt.setString(6,ISBN);
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    //用户权限
    public void change(int id,String account,String name,String password,String power,String love){
        //修改用户的个人信息
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql = "update people set account = ?,name = ?,password = ? ,power = ?,love = ? where userid = ? ";
            stmt = conn.prepareStatement(sql);
            //向数据库插入数据
            stmt.setString(1, account);
            stmt.setString(2, name);
            stmt.setString(3, password);
            stmt.setString(4, power);
            stmt.setString(5, love);
            stmt.setInt(6, id);
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    public void insertUser(String account, String name, String password,String power,String like) {
        /*向数据库中插入用户信息，用于注册*/
        int id = 0;
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql1 = "select userid from people order by userid";
            String sql2 = "insert into people(userid,account,name,password,power,love) values (?,?,?,?,?,?)";
            pst = conn.createStatement();
            ResultSet rs = pst.executeQuery(sql1);
            stmt = conn.prepareStatement(sql2);
            while (rs.next()) {
                //用户自动赋予ID，在当前表中前一个用户ID基础上加一
                id = rs.getInt("userid") + 1;
            }
            //向数据库插入数据
            stmt.setInt(1, id);
            stmt.setString(2, account);
            stmt.setString(3, name);
            stmt.setString(4, password);
            stmt.setString(5,power);
            stmt.setString(6,like);
            stmt.executeUpdate();
            // 完成后关闭
            rs.close();
            pst.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public String check_account(String loginAccount) {
        /*检查当前账号是否在数据库中*/
        String trans_account = null;
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql;
            sql = "select account from people where account = ?";//sql语句
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, loginAccount);
            rs = stmt.executeQuery();
            // 展开结果集数据库
            while (rs.next()) {
                // 通过字段检索
                trans_account = rs.getString("account");
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return trans_account;
    }

    public String getPassword(String loginAccount) {
        /*返回当前账号的密码用于检验是否匹配*/
        String trans_password = null;
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql;
            sql = "select password from people where account = ?";//sql语句
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, loginAccount);
            rs = stmt.executeQuery();
            // 展开结果集数据库
            while (rs.next()) {
                // 通过字段检索
                trans_password = rs.getString("password");
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return trans_password;
    }

    public void searchBook(String bookName) {
        /*获取要搜索的图书信息*/
        dataRoom book = new dataRoom();
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql;
            sql = "select * from book where bName like ?";//sql语句
            stmt = conn.prepareStatement(sql);
            stmt.setString(1,"%"+bookName+"%");
            rs = stmt.executeQuery();
            // 展开结果集数据库
            while (rs.next()) {
                // 通过字段检索
                    book.setInformation(rs.getString("ISBN"), rs.getString("bName"),
                            rs.getString("aName"), rs.getString("sign"), rs.getInt("number"));
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    /*用于借阅和归还图书*/
    public void browBook(String bookName, int bookNum) {
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql;
            sql = "update book set number = number - ? where bName = ?";//sql语句
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, bookNum);
            stmt.setString(2, bookName);
            stmt.execute();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    //用户个人当前的借阅记录，归还后删除对应记录
    public void browInformation(int userId, String bookName, String author, String sign, boolean fig) {
        //fig == true 代表借阅 fig == false 代表归还
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql;
            // 执行查询
            if (fig) {
                sql = "insert into browinformation(id, bookName, Author, sign) values (?,?,?,?)";
                stmt = conn.prepareStatement(sql);
                //向数据库插入数据
                stmt.setInt(1, userId);
                stmt.setString(2, bookName);
                stmt.setString(3, author);
                stmt.setString(4, sign);
            } else {
                sql = "delete from browinformation where id = ? and bookName = ?";
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, userId);
                stmt.setString(2, bookName);
            }
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    //用于上传借阅记录信息
    public void browRecord(int userId, String userAccount, String currentTime, String bookName, String bookSign,boolean sign) {

        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql;
            // 执行查询
            if (sign) {
                //点击借阅按钮
                sql = "insert into browRecord(browId, browName, browTime, browBook,sign) values (?,?,?,?,?)";
            } else {
                //点击归还按钮
                sql = "insert into browreturn(returnId,returnName,returnTime,returnBook,sign) values(?,?,?,?,?)";
            }
            stmt = conn.prepareStatement(sql);
            //向数据库插入数据
            stmt.setInt(1, userId);
            stmt.setString(2, userAccount);
            stmt.setString(3, currentTime);
            stmt.setString(4, bookName);
            stmt.setString(5,bookSign);
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    //判断热门图书表是否包含本书
    public boolean judge(String isbn){
        //若不包含则读者借阅本书时，将本书插入热门图书表记录
        //若包含则更新本书的累计借阅次数
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql1 = "select id from brownum where id = ?";
            stmt = conn.prepareStatement(sql1);
            stmt.setString(1,isbn);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String re = rs.getString("id") ;
                return true;
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return false;
    }
    public boolean judge_sign(String bookSign){
        //判断标签统计表是否包含本标签
        //若不包含则读者借阅书籍时，将标签插入标签统计表记录
        //若包含则更新本标签的累计借阅次数
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql1 = "select sign from sign where sign= ?";
            stmt = conn.prepareStatement(sql1);
            stmt.setString(1,bookSign);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String re = rs.getString("sign") ;
                return true;
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return false;
    }
    public int count_sign(String bookSign,int uid){
        //计算用户当前标签的总借阅数
        int num = 0;
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql1 = "select count(browId) as num from browrecord where  sign = ? and browId = ?";
            stmt = conn.prepareStatement(sql1);
            stmt.setString(1,bookSign);
            stmt.setInt(2,uid);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                num = rs.getInt("num");
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return num;
    }
    public String  select_sign(){
        //计算用户当前标签的总借阅数
        String book = null;
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql1 = "select sign from sign order by num desc limit 1";
            stmt = conn.prepareStatement(sql1);
            rs = stmt.executeQuery();
            while (rs.next()) {
                book = rs.getString("sign");
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return book;
    }
    public void update_Sign(int userid,String bookSign,int number) {
        /*更新当前标签的总借阅数*/
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql = "update sign set num = ? where sign = ? and uid = ?";
            stmt = conn.prepareStatement(sql);
            //向数据库插入数据
            stmt.setInt(1, number);
            stmt.setString(2,bookSign);
            stmt.setInt(3,userid);
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    public void insert_Sign(int userid,String bookSign,int number){
        //向推荐插入记录
        String sql2;
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            sql2 = "insert into sign(uid, sign, num) VALUES (?,?,?)";
            stmt = conn.prepareStatement(sql2);
            stmt.setInt(1,userid);
            stmt.setString(2,bookSign);
            stmt.setInt(3,number);
            //向数据库插入数据
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    public int count_browNum(String bookName){
        //计算本书的总借阅数
        int num = 0;
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            String sql1 = "select count(browBook) as num from browRecord where browBook = ?";
            stmt = conn.prepareStatement(sql1);
            stmt.setString(1,bookName);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                num = rs.getInt("num") + 1;
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return num;
    }
    public void insert_browNum(String bookIsbn,String name,int number){
        //向热门图书插入记录
        String sql2;
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            sql2 = "insert into brownum(id, bookname, browcount) VALUES (?,?,?)";
            stmt = conn.prepareStatement(sql2);
            stmt.setString(1,bookIsbn);
            stmt.setString(2,name);
            stmt.setInt(3,number);
            //向数据库插入数据
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
    public void upDate_browNum(String bookIsbn,String name,int number){
        //更新热门图书的记录
        String sql;
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            sql = "update brownum set browCount = ? where id = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1,number);
            stmt.setString(2,bookIsbn);
            //向数据库插入数据
            stmt.executeUpdate();
            // 完成后关闭
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public boolean check_book(int userid) {
        //检查用户当前在借的书，如果有则无法注销账号
        try {
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // 执行查询
            String sql;
            sql = "select bookName from browInformation where id =?";//sql语句
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userid);
            rs = stmt.executeQuery();
            // 展开结果集数据库
            while (rs.next()) {
                return true;
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            // 处理 JDBC 错误
            se.printStackTrace();
        } catch (Exception e) {
            // 处理 Class.forName 错误
            e.printStackTrace();
        } finally {
            // 关闭资源
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            }// 什么都不做
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return false;
    }
    public void delete(int id,boolean sign){
        /*管理员删除用户*/
        try {
            Class.forName(JDBC_DRIVER);
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            //连接people和brow表，并删除两表中当前账号的记录
            String sql = "delete from people where userid = ?";
            String sql2 = "delete from browrecord where browId = ?";
            String sql3 = "delete from browreturn where returnId = ?";
            if(sign){
                String sql4 = "delete from browInformation where Id = ?";
                stmt4 = conn.prepareStatement(sql4);
                stmt4.setInt(1, id);
                stmt4.executeUpdate();
            }
            stmt = conn.prepareStatement(sql);
            stmt2 = conn.prepareStatement(sql2);
            stmt3 = conn.prepareStatement(sql3);
            stmt.setInt(1, id);
            stmt2.setInt(1, id);
            stmt3.setInt(1, id);
            stmt.executeUpdate();
            stmt2.executeUpdate();
            stmt3.executeUpdate();
        }catch(SQLException se){
            se.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if(stmt!=null) stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}
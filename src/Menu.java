import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
//首页
public class Menu extends JFrame{
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/library?" +
            "useSSL=false & allowPublicKeyRetrieval=true & serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";
    Connection conn = null;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    JMenuBar menuBar;//菜单容器
    JMenu information,person,like;//information个人系统，person说明
    JMenuItem center,brow,mybook,exitSystem,delete,transport,likeBook;//center个人中心，brow借阅中心，mybook个人书架，delete注销，exitSystem退出登录
    JMenuItem people,system;//people人员说明，system系统说明
    JPanel panel1,panel2,panel3,panel4;//panel1搜索框，panel2图书，panel3翻页
    JTextField searchField;
    JButton searchButton;
    JTextArea bookArea;
    //initAccount 仅用于判断是否登录，initAccount等于NULL表示未登录
    //nowAccount 用于获取当前登录的账号
    String nowAccount;
    public static String initAccount;
    //代码冗余
    ActionListener menuListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("个人中心")) {
                if(initAccount == null) {
                    Login login = new Login();
                    login.setVisible(true);
                }
                else {
                    judge_people();
                }
            }
        }
    };
    ActionListener exitListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("退出登录") && initAccount != null) {
                initAccount = null;
                new dataRoom().setInformation(null,null,null,null,0);
            }
            else
                System.out.println("您当前还未登录账号！");
        }
    };
    ActionListener del = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
                new dataRoom().getPerson(Login.account);
                dataRoom accountData = new dataRoom();
            if(e.getActionCommand().equals("注销账号") && initAccount == null) {
                System.out.println("您当前还未登录账号！");
            }
            else if(accountData.check_book(dataRoom.userid)){
                System.out.println("您当前有未归还的图书，无法注销!");
            }
            else {
                new dataRoom().getPerson(Login.account);
                boolean sign = false;
                accountData.delete(dataRoom.userid,false);
                initAccount = null;
                System.out.println("账号注销成功");
            }
        }
    };
    ActionListener searchBook = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("搜索")) {
                dataRoom bookData = new dataRoom();
                bookData.searchBook(searchField.getText());
                if(searchField.getText().isEmpty()){
                    bookArea.setText("");
                    bookArea.append("您还未输入搜索内容!");
                }
                else if (bookData.BookIsbn == null) {
                    bookArea.setText("");
                    bookArea.append("图书馆内无本书，请联系管理员购买！");
                }
                else {
                    bookArea.setText("");
                    bookArea.append("图书编号\t\t图书名称\t\t作者\t\t标签\t\t数量"+"\n");
                    try {
                        // 注册 JDBC 驱动
                        Class.forName(JDBC_DRIVER);
                        // 打开链接
                        conn = DriverManager.getConnection(DB_URL, USER, PASS);
                        String sql;
                        sql = "select * from book where bName like ?";//sql语句
                        stmt = conn.prepareStatement(sql);
                        stmt.setString(1,"%"+searchField.getText()+"%");
                        rs = stmt.executeQuery();
                        // 展开结果集数据库
                        while (rs.next()) {
                            // 通过字段检索
                            bookArea.append(rs.getString("ISBN")+"\t\t"+rs.getString("bName")+"\t\t"+rs.getString("aName")
                                    +"\t\t"+rs.getString("sign")+"\t\t"+rs.getInt("number")+"\n");
                        }
                        // 完成后关闭
                        rs.close();
                        stmt.close();
                        conn.close();
                    } catch (SQLException se) {
                        // 处理 JDBC 错误
                        se.printStackTrace();
                    } catch (Exception be) {
                        // 处理 Class.forName 错误
                        be.printStackTrace();
                    } finally {
                        // 关闭资源
                        try {
                            if (stmt != null) stmt.close();
                        } catch (SQLException se2) {
                        }// 什么都不做
                        try {
                            if (conn != null) conn.close();
                        } catch (SQLException se) {
                            se.printStackTrace();
                        }
                    }
                    bookData.setInformation(null,null,null,null,0);
                }
            }
        }
    };
    ActionListener browBook = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("借阅中心") && initAccount != null) {
                browCenter browcenter = new browCenter();
                browcenter.setVisible(true);
            }
            else {
                System.out.println("请先登录再使用本功能！");
            }
        }
    };
    ActionListener Like = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("热门图书") ) {
                bookArea.setText("");
                bookArea.append("本馆最近热门图书：\n");
                printBook(2);
            }
        }
    };
    ActionListener bookshelf = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("我的书架") && initAccount != null) {
                new dataRoom().getPerson(Login.account);
                shelf myShelf = new shelf();
                myShelf.setVisible(true);
                myShelf.printShelf(dataRoom.userid);
            }
            else {
                System.out.println("请先登录再使用本功能！");
            }
        }
    };
    ActionListener mainListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("首页")) {
               bookArea.setText("");//清除无书的输出
               printBook(1);
               searchField.setText("");
            }
        }
    };
    public Menu(String s,int x,int y,int w,int h){
        setLocation(x,y);
        setSize(w,h);
        init(s);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public void init(String s) {
        menuBar=new JMenuBar();
        setTitle("个人中心");
        setPage();//首页
        setPerson();//个人系统
        setLike();//热门图书
        setInformation();//说明
        menuBar.add(person);
        menuBar.add(information);
        menuBar.add(like);
        setJMenuBar(menuBar);
    }
    //设置首页
    public void setPage(){
        searchField = new JTextField(50);
        searchButton = new JButton("搜索");
        panel1=new JPanel();
        panel2=new JPanel();
        panel3=new JPanel();
        panel4=new JPanel();
        panel4.setLayout(new FlowLayout(FlowLayout.LEFT,40,30));
        bookArea = new JTextArea(20,110);
        Font font = new Font("宋体",Font.PLAIN,20);
        bookArea.setFont(font);
        panel4.add(bookArea);
        Button b1=new Button("首页");
        b1.addActionListener(mainListener);
        Button b2=new Button("上一页");
        Button b3=new Button("下一页");
        Button b4=new Button("最后一页");
        panel1.add(searchField);
        panel1.add(searchButton);
        searchButton.addActionListener(searchBook);
        panel3.add(b1);
        panel3.add(b2);
        panel3.add(b3);
        panel3.add(b4);
        add(panel1,BorderLayout.NORTH);
        add(panel4,BorderLayout.CENTER);
        add(panel3,BorderLayout.SOUTH);
    }
    public void judge_people(){
        new dataRoom().getPerson(Login.account);
        if(dataRoom.userid == 1){
            superPage superpage = new superPage();
            superpage.setVisible(true);
        }
        else if(dataRoom.userid < 1000){
            managePage managepage = new managePage();
            managepage.setVisible(true);
        }
        else {
            page Page = new page();
            Page.setVisible(true);
        }
    }
    public void printBook(int n){
            try{
                // 注册 JDBC 驱动
                Class.forName(JDBC_DRIVER);
                // 打开链接
                conn = DriverManager.getConnection(DB_URL,USER,PASS);
                // 执行查询
                String sql = null;
                switch (n) {
                    case 1:bookArea.append("图书编号\t\t图书名称\t\t\t作者\t\t标签"+"\n");
                    sql = "select ISBN, bName, aName, sign from book";break;//sql语句
                    case 2:bookArea.append("图书编号\t\t图书名称\t\t\t\t借阅次数"+"\n");
                        sql = "select * from brownum order by browCount desc limit 5";break;
                    default:System.out.println("数据错误");
                }
                stmt= conn.prepareStatement(sql);
                rs = stmt.executeQuery();
                // 展开结果集数据库
                while(rs.next()){
                    if(n == 1) {
                        bookArea.append(rs.getString("ISBN") + "\t\t" + rs.getString("bName")
                                + "\t\t" + rs.getString("aName") + "\t\t" + rs.getString("sign") + "\n");
                    }
                    else if(n == 2){
                        bookArea.append(rs.getString("id") + "\t\t" + rs.getString("bookName")
                                + "\t\t\t" + rs.getString("browCount")  + "\n");
                    }
                }
                // 完成后关闭
                rs.close();
                stmt.close();
                conn.close();
            }catch(SQLException se){
                // 处理 JDBC 错误
                se.printStackTrace();
            }catch(Exception e){
                // 处理 Class.forName 错误
                e.printStackTrace();
            }finally{
                // 关闭资源
                try{
                    if(stmt!=null) stmt.close();
                }catch(SQLException se2){
                }// 什么都不做
                try{
                    if(conn!=null) conn.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }
            }
    }
    //设置个人系统
    public void setPerson() {
        person = new JMenu("个人系统");
        center = new JMenuItem("个人中心");
        brow = new JMenuItem("借阅中心");
        mybook = new JMenuItem("我的书架");
        delete = new JMenuItem("注销账号");
        exitSystem = new JMenuItem("退出登录");
        center.addActionListener(menuListener);
        exitSystem.addActionListener(exitListener);
        brow.addActionListener(browBook);
        mybook.addActionListener(bookshelf);
        delete.addActionListener(del);
        person.add(center);
        person.add(brow);
        person.add(mybook);
        person.add(delete);
        person.add(exitSystem);
    }
    public void setLike(){
        like = new JMenu("热门");
        likeBook = new JMenuItem("热门图书");
        likeBook.addActionListener(Like);
        like.add(likeBook);
    }
    //设置说明
    public void setInformation(){
        information=new JMenu("说明");
        people=new JMenuItem("人员说明");
        people.addActionListener(e -> JOptionPane.showMessageDialog(this,
                "Java：徐明虎，李启明\n数据库：郭国芳，王启迪",
                "开发人员",JOptionPane.INFORMATION_MESSAGE));
        system=new JMenuItem("系统说明");
        system.addActionListener((e -> JOptionPane.showMessageDialog(this,
                "数据库：MySQL\n开发平台：Idea、Eclipse\n开发语言：Java",
                "开发系统",JOptionPane.INFORMATION_MESSAGE)
        ));
        information.add(people);
        information.add(system);
    }
}
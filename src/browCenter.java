import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//借阅中心页面
public class browCenter extends JFrame{
    JTextField browName,browNum;
    JLabel browNameLabel,browNumLabel;
    JButton brow,restore;
    JPanel panel;
    boolean fig;
    public String record (){
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf =
                new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String Time = sdf.format(dt);
        new dataRoom().browBook(browName.getText(), Integer.parseInt(browNum.getText()));
        new dataRoom().getPerson(Login.account);
        return Time;
    }
    ActionListener bw = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //存在BUG，先借一本数量为0的书输出正常，但是之后再输入一本不存在的书
                    //输出结果同数量为0的输出
                    new dataRoom().getPerson(Login.account);
                    if (e.getActionCommand().equals("借阅")) {
                        fig = true;
                        dataRoom bookData = new dataRoom();
                        bookData.searchBook(browName.getText());
                        bookData.getLimitNum(dataRoom.userid);
                        if (bookData.BookIsbn == null) {
                            System.out.println("图书馆内无本书，请联系管理员购买！");
                        }
                        else if (bookData.BookNumber == 0) {
                            System.out.println("本书已借完，请借阅其它书籍或等待用户还书！");
                        }
                        else if (Integer.parseInt(browNum.getText()) != 1) {
                            System.out.println("借阅数量不合理，每本书每次只能借一本，请重新输入您的借书数量！");
                        }
                        else if (bookData.judge_book(browName.getText(),dataRoom.userid)) {
                            System.out.println("您已经借过本书，无需再借!");
                        }
                        else if (bookData.limitNum > 2) {
                            System.out.println("您当前借阅次数已达到上限！");
                        }
                        else {
                            String browTime = record();
                            bookData.browRecord(dataRoom.userid,Login.account,browTime,browName.getText(),dataRoom.Sign,fig);
                            bookData.browInformation(dataRoom.userid,browName.getText(),dataRoom.Author,dataRoom.Sign,fig);
                            int num = bookData.count_browNum(browName.getText());
                            int number = bookData.count_sign(dataRoom.Sign,dataRoom.userid);
                            if(bookData.judge(dataRoom.BookIsbn)) {
                                bookData.upDate_browNum(dataRoom.BookIsbn, browName.getText(), num);
                            }
                            else {
                                bookData.insert_browNum(dataRoom.BookIsbn, browName.getText(), num);
                                System.out.println("书籍《" + browName.getText() + "》借阅成功");
                                bookData.limitNum++;
                            }
                            if(bookData.judge_sign(dataRoom.Sign)){
                                bookData.update_Sign(dataRoom.userid,dataRoom.Sign,number);
                            }
                            else
                                bookData.insert_Sign(dataRoom.userid,dataRoom.Sign,number);
                        }
                    }
                    dispose();
                }
            };
    ActionListener rs = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (e.getActionCommand().equals("归还")) {
                        fig = false;
                        dataRoom bookData = new dataRoom();
                        bookData.searchBook(browName.getText());
                        if(!bookData.judge_book(browName.getText(),dataRoom.userid)) {
                            System.out.println("您并未借这本书，无需归还");
                        } else{
                            new dataRoom().browBook(browName.getText(), -1 * Integer.parseInt(browNum.getText()));
                            String returnTime = record();
                            bookData.browRecord(dataRoom.userid,Login.account,returnTime,browName.getText(),dataRoom.Sign,fig);
                            bookData.browInformation(dataRoom.userid,browName.getText(),dataRoom.Author,dataRoom.Sign,fig);
                            System.out.println("您已归还本书，谢谢");
                            System.out.println("书籍《"+browName.getText()+"》归还成功");
                            dispose();
                            }
                        }
                    }
                    //dispose();
            };
    public browCenter() {
        setTitle("借阅中心");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        browNameLabel = new JLabel("借阅图书名称：");
        browNumLabel = new JLabel("借阅数量：");
        browName = new JTextField();
        browNum = new JTextField();
        brow = new JButton("借阅");
        restore = new JButton("归还");
        brow.addActionListener(bw);
        restore.addActionListener(rs);
        panel.add(browNameLabel);
        panel.add(browNumLabel);
        panel.add(browName);
        panel.add(browNum);
        panel.add(brow);
        panel.add(restore);
        add(panel, BorderLayout.CENTER);
        }
}

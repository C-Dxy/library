import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//登录注册页面
public class Login extends JFrame {
    JTextField accountField;
    JPanel panel;
    JLabel accountLabel,passwordLabel;
    JPasswordField passwordField;
    JButton loginButton, registerButton;
    public static String account;
    public void  setAccount(String a) {
        account = a;
    }
    ActionListener lg=new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            // 处理登录的逻辑，可以获取输入的用户名和密码进行验证
            setAccount(accountField.getText());
            String password = new String(passwordField.getPassword());
            Menu.initAccount = accountField.getText();
            //判断账号是否存在以及密码是否匹配
            //如果当前账号在数据库中不存在则getAccount==null，否则getAccount == account
            //judgePassword 为当前登录账号在数据库中设置的密码
            String getAccount = new dataRoom().check_account(account);
            String judgePassword = new dataRoom().getPassword(account);
            new dataRoom().getPerson(account);
            boolean judge = false;
            //getAccount == null 说明数据库中没有该账号
            //judge ==  false 说明当前输入的密码与原密码不一致
            if (getAccount != null) {
                judge = password.equals(judgePassword);
            }
            if (getAccount == null) {
                System.out.println("账号不存在，请先注册账号！");
                Menu.initAccount = null;
            } else if (!judge) {
                System.out.println("密码不正确，请重新输入！");
                Menu.initAccount = null;
            } else {
                System.out.println("账号"+account+"登录成功");
                if(dataRoom.userid == 1){
                    superPage superpage = new superPage();
                    superpage.setVisible(true);
                }
                else if(dataRoom.userid < 1000){
                    managePage managepage = new managePage();
                    managepage.setVisible(true);
                }
                else {
                    page Page = new page();
                    Page.setVisible(true);
                }
            }
            // 登录成功后关闭当前登录界面
            dispose();
        }
    };
    public Login() {
        setTitle("用户登录");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        accountLabel = new JLabel("账号：");
        accountField = new JTextField();
        passwordLabel = new JLabel("密码：");
        passwordField = new JPasswordField();
        loginButton = new JButton("登录");
        registerButton = new JButton("注册");
        loginButton.addActionListener(lg);
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Register register = new Register();
                register.setVisible(true);
            }
        });
        panel.add(accountLabel);
        panel.add(accountField);
        panel.add(passwordLabel);
        panel.add(passwordField);
        panel.add(loginButton);
        panel.add(registerButton);
        add(panel, BorderLayout.CENTER);
    }
}
class Register extends JFrame {
    JTextField accountField,usernameField,powerField,likeField;
    JPasswordField passwordField,passwordField2;
    JPanel panel;
    JLabel accountLabel,usernameLabel,power,like,passwordLabel,password;
    JButton registerButton;
    public Register() {
        setTitle("用户注册");
        setSize(300, 300);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        panel = new JPanel();
        panel.setLayout(new GridLayout(7, 3));
        accountLabel = new JLabel("手机号：");
        accountField = new JTextField();
        usernameLabel = new JLabel("用户名：");
        usernameField = new JTextField();
        power = new JLabel("专业：");
        powerField = new JTextField();
        like = new JLabel("爱好：");
        likeField =  new JTextField();
        passwordLabel = new JLabel("密码：");
        passwordField = new JPasswordField();
        password = new JLabel("确认密码：");
        passwordField2 = new JPasswordField();
        registerButton = new JButton("注册");
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 处理注册的逻辑，可以获取输入的用户名和密码进行注册操作
                String account = accountField.getText();
                String username = usernameField.getText();
                String password = new String(passwordField.getPassword());
                String password2 = new String(passwordField2.getPassword());
                String power = powerField.getText();
                String like = likeField.getText();
                if(account.isEmpty() || username.isEmpty()){
                    System.out.println("用户名或手机号不能为空！");
                }
                else if(password2.equals(password)) {
                    //点击注册后信息提交
                    new dataRoom().insertUser(account, username, password,power,like);
                    // 注册成功后关闭当前注册界面，跳转到登录界面
                    dispose();
                }
                else System.out.println("两次密码不一致，请重新输入！");
            }
        });
        panel.add(accountLabel);
        panel.add(accountField);
        panel.add(usernameLabel);
        panel.add(usernameField);
        panel.add(power);
        panel.add(powerField);
        panel.add(like);
        panel.add(likeField);
        panel.add(passwordLabel);
        panel.add(passwordField);
        panel.add(password);
        panel.add(passwordField2);
        panel.add(registerButton);
        add(panel, BorderLayout.CENTER);
    }
}
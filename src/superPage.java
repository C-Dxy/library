import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//超级管理员页面
public class superPage extends JFrame{
    JMenuBar bar;
    JMenu menu;
    JMenuItem delete,create;
    ActionListener delAct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("删除管理员")) {
                delMana delmana = new delMana();
                delmana.setVisible(true);
            }
        }
    };
    ActionListener createAct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("创建管理员")) {
                createMana create = new createMana();
                create.setVisible(true);
            }
        }
    };
    public superPage(){
        setTitle("超级管理员中心");
        setSize(800,500);
        setPage();
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    public void setPage(){
        bar = new JMenuBar();
        menu = new JMenu("权限");
        delete = new JMenuItem("删除管理员");
        delete.addActionListener(delAct);
        create = new JMenuItem("创建管理员");
        create.addActionListener(createAct);
        menu.add(delete);
        menu.add(create);
        bar.add(menu);
        setJMenuBar(bar);
    }
}
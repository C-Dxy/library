import javax.swing.*;
import java.awt.*;
import java.sql.*;
//用户书架页面
public class shelf extends JFrame{
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/library?" +
            "useSSL=false & allowPublicKeyRetrieval=true & serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";
    Connection conn = null;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    JTextArea shelfArea;
    JPanel panel4;
    public shelf (){
        setTitle("我的书架");
        setSize(800, 500);
        panel4 = new JPanel();
        shelfArea = new JTextArea(20,110);
        Font font = new Font("宋体",Font.PLAIN,20);
        panel4.setLayout(new FlowLayout(FlowLayout.LEFT,40,30));
        shelfArea.setFont(font);
        panel4.add(shelfArea);
        add(panel4,BorderLayout.CENTER);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    public void printShelf(int userId){
        shelfArea.append("您当前借阅的书籍有：\n");
        shelfArea.append("图书名称\t\t作者\t\t标签\n");
        try{
                // 注册 JDBC 驱动
                Class.forName(JDBC_DRIVER);
                // 打开链接
                conn = DriverManager.getConnection(DB_URL,USER,PASS);
                // 执行查询
                String sql;
                sql = "select bookName, Author, sign from browinformation where id =?";//sql语句
                stmt= conn.prepareStatement(sql);
                stmt.setInt(1, userId);
                rs = stmt.executeQuery();
                // 展开结果集数据库
                while(rs.next()){
                    shelfArea.append(rs.getString("bookName")
                            +"\t\t"+ rs.getString("Author")+"\t\t"+rs.getString("sign")+"\n");
                }
                // 完成后关闭
                rs.close();
                stmt.close();
                conn.close();
            }catch(SQLException se){
                // 处理 JDBC 错误
                se.printStackTrace();
            }catch(Exception e){
                // 处理 Class.forName 错误
                e.printStackTrace();
            }finally{
                // 关闭资源
                try{
                    if(stmt!=null) stmt.close();
                }catch(SQLException se2){
                }// 什么都不做
                try{
                    if(conn!=null) conn.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }
            }
        }
}

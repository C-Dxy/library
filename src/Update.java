import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//上传资源页面
public class Update extends JFrame {
    JPanel panel;
    Label ISBNLabel,bNameLabel,authorLabel,numberLabel,signLabel;
    JTextField ISBNFiled,nameField,authorField,signField,numberField;
    JButton submit;
    boolean chooce;
    ActionListener sAL=new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("提交")) {
                    String ISBN = ISBNFiled.getText();
                    String bName = nameField.getText();
                    String aName = authorField.getText();
                    int number = Integer.parseInt(numberField.getText());
                    String sign = signField.getText();
                    new dataRoom().insertBook(ISBN, bName, aName, sign, number,chooce);
                    dispose();
            }
        }
    };
    public Update(boolean choice) {
        chooce = choice;
        if(chooce)
            setTitle("上传资源");
        else
            setTitle("更新图书");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        panel=new JPanel();
        panel.setLayout(new GridLayout(6, 2));
        ISBNLabel=new Label("书籍序列");
        bNameLabel=new Label("书籍名称");
        authorLabel=new Label("书籍作者");
        signLabel=new Label("书籍标签");
        numberLabel=new Label("书籍数量");
        submit=new JButton("提交");
        submit.addActionListener(sAL);
        ISBNFiled=new JTextField();
        nameField=new JTextField();
        authorField=new JTextField();
        signField=new JTextField();
        numberField=new JTextField();
        panel.add(ISBNLabel);
        panel.add(ISBNFiled);
        panel.add(bNameLabel);
        panel.add(nameField);
        panel.add(authorLabel);
        panel.add(authorField);
        panel.add(signLabel);
        panel.add(signField);
        panel.add(numberLabel);
        panel.add(numberField);
        panel.add(submit);
        add(panel,BorderLayout.CENTER);
    }
}
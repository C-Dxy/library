--用户表
create table people(
    userid int primary key ,
    account char(11) not null unique ,
    name varchar(20) not null ,
    password varchar(20) not null,
    power varchar(10),
    love varchar(10)
);
--图书表
create table book(
    ISBN char(4) primary key ,
    bName varchar(20) not null ,
    aName varchar(10)  ,
    sign varchar(10),
    number int,
    foreign key  book(aName) references author(aName)
);
--作者表
create table author(
    aName varchar(10) primary key,
    nation varchar(10)
);
--借阅记录表
create table browRecord(
    browId int,
    browName varchar(15),
    browTime DATETIME DEFAULT NOW() primary key ,
    browBook varchar(20) not null,
    sign varchar(10)
);
--归还记录表
create table browReturn(
    returnId int,
    returnName varchar(15),
    returnTime DATETIME DEFAULT NOW() primary key ,
    returnBook varchar(20) not null,
    sign varchar(10)
);
--借阅信息表
create table browInformation(
    Id int  ,
    bookName varchar(20) ,
    Author varchar(10),
    sign varchar(10),
    primary key (Id,bookName)
);
--借阅数量表
create table browNum(
   Id char(4) primary key ,
   bookName varchar(10) ,
   browCount int,
   foreign key  browNum(Id) references book(ISBN)
);
--管理员表
create table post(
    emId int primary key ,
    emAccount char(10),
    emName varchar(10),
    emPassword char(10)
);
--标签数量表
create table sign(
    uid int ,
    sign varchar(10),
    num int,
    primary key (uid,sign)
);
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//上传资源页面
public class upAuthor extends JFrame {
    JPanel panel;
    Label authorLabel,nationLabel;
    JTextField authorField,nationField;
    JButton submit;
    ActionListener sAL=new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("提交")) {
                String aName = authorField.getText();
                String nation = nationField.getText();
                new dataRoom().insertAuthor(aName, nation);
                dispose();
            }
        }
    };
    public upAuthor() {
        setTitle("上传作者");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        panel=new JPanel();
        panel.setLayout(new GridLayout(3, 2));
        authorLabel=new Label("书籍作者");
        nationLabel = new Label("作者国籍");
        submit=new JButton("提交");
        submit.addActionListener(sAL);
        authorField=new JTextField();
        nationField=new JTextField();
        panel.add(authorLabel);
        panel.add(authorField);
        panel.add(nationLabel);
        panel.add(nationField);
        panel.add(submit);
        add(panel,BorderLayout.CENTER);
    }
}

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class delMana extends JFrame{
    JPanel panel;
    JLabel idLabel;
    JTextField idField;
    JButton delButton;
    public delMana(){
        setTitle("删除管理员");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        panel = new JPanel();
        panel.setLayout(new GridLayout(2, 3));
        idLabel = new JLabel("管理员ID：");
        idField = new JTextField();
        delButton = new JButton("删除");
        panel.add(idLabel);
        panel.add(idField);
        panel.add(delButton);
        add(panel, BorderLayout.CENTER);
        delButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (idField.getText().isEmpty()){
                    System.out.println("请输入内容!");
                }
                else{
                    int id = Integer.parseInt(idField.getText());
                    new dataRoom().getPerson(Login.account);
                    new dataRoom().delMana(id);
                    System.out.println("该管理员已删除");
                    dispose();
                }
            }
        });
    }
}

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class delData extends JFrame{
        //删除借阅记录页面
        JPanel panel;
        Label IDLabel,bookLabel,judgeLabel;
        JTextField IDFiled,boolField,judgeField;
        JButton del;
        ActionListener dAL=new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals("删除")) {
                    int ID = Integer.parseInt(IDFiled.getText());
                    String book = boolField.getText();
                    new dataRoom().delRecode(ID,book);
                    dispose();
                }
            }
        };
        public delData() {
            setTitle("删除记录");
            setSize(300, 200);
            setLocationRelativeTo(null);
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            panel=new JPanel();
            panel.setLayout(new GridLayout(4, 2));
            IDLabel=new Label("借阅ID");
            bookLabel = new Label("借阅书籍");
            judgeLabel = new Label("记录选择");
            judgeField = new JTextField();
            del = new JButton("删除");
            del.addActionListener(dAL);
            IDFiled=new JTextField();
            boolField = new JTextField();
            panel.add(IDLabel);
            panel.add(IDFiled);
            panel.add(bookLabel);
            panel.add(boolField);
            panel.add(del);
            add(panel,BorderLayout.CENTER);
        }
}

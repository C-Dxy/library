import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//修改个人信息页面

public class changePage extends JFrame{
    JTextField accountField,usernameField,powerField,likeField;
    JPasswordField passwordField,passwordField2;
    JPanel panel2;
    JLabel accountLabel,usernameLabel,passwordLabel,passwordLabel2,powerLabel,likeLabel;
    JButton changeButton;
    JTextArea text;
    public String cname,cpw,cpower,clove;
    boolean result = true;
    public changePage(){
        setTitle("修改页面");
        setSize(300, 300);
        setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        if(result)
            setPage();
        else{
            page p = new page();
            p.setVisible(true);
        }
    }
    public void setPage(){
        panel2 = new JPanel();
        panel2.setLayout(new GridLayout(8, 3));
        accountLabel = new JLabel("手机号：");
        accountField = new JTextField();
        usernameLabel = new JLabel("用户名：");
        usernameField = new JTextField();
        powerLabel = new JLabel("专业");
        powerField = new JTextField();
        likeLabel = new JLabel("爱好");
        likeField =  new JTextField();
        passwordLabel = new JLabel("密码：");
        passwordField = new JPasswordField();
        passwordLabel2 = new JLabel("确认密码：");
        passwordField2 = new JPasswordField();
        changeButton = new JButton("修改");
        changeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new dataRoom().getPerson(Login.account);
                setInformation();
                new dataRoom().change(dataRoom.userid,Login.account,cname,cpw,cpower,clove);
                result = false;
                System.out.println("个人信息修改成功");
                dispose();
            }
        });
        panel2.add(accountLabel);
        panel2.add(accountField);
        panel2.add(usernameLabel);
        panel2.add(usernameField);
        panel2.add(powerLabel);
        panel2.add(powerField);
        panel2.add(likeLabel);
        panel2.add(likeField);
        panel2.add(passwordLabel);
        panel2.add(passwordField);
        panel2.add(passwordLabel2);
        panel2.add(passwordField2);
        panel2.add(changeButton);
        add(panel2, BorderLayout.CENTER);
    }
    public void newPage(){
        text = new JTextArea(5,110);
        Font font = new Font("宋体",Font.PLAIN,20);
        text.setFont(font);
        panel2.add(text);
        add(panel2,BorderLayout.SOUTH);
    }

    public void setInformation(){
        cname = usernameField.getText();
        cpw = new String(passwordField.getPassword());
        cpower = powerField.getText();
        clove = likeField.getText();
    }
}

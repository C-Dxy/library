import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
//管理员页面
//点击搜索后会显示当前ID借阅记录
public class managePage extends JFrame {
    JMenuBar bar;
    JMenu powerful;
    JMenuItem delData,delUser,showData,transport,upInf,change,delBook;
    JTextArea area;
    JTextField searchField;
    static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost:3306/library?" +
            "useSSL=false & allowPublicKeyRetrieval=true & serverTimezone=UTC";
    static final String USER = "root";
    static final String PASS = "123456";
    Connection conn = null;
    PreparedStatement stmt = null;
    ResultSet rs = null;
    public int choice = 0;
    ActionListener showAct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("查看归还记录")) {
                choice = 1;
            }
        }
    };
    ActionListener delDataAct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("删除记录")) {
                delData delData1 = new delData();
                delData1.setVisible(true);
            }
        }
    };
    ActionListener delUserAct = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getActionCommand().equals("删除用户")) {
                delUser del = new delUser(true);
                del.setVisible(true);
            }
        }
    };
    ActionListener searchRecord = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("搜索")) {
                int id = Integer.parseInt(searchField.getText());
                if (choice == 0)
                    printRecord(id, true);
                if (choice == 1)
                    printRecord(id,false);
            }
        }
    };
    ActionListener updateListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("上传资源")) {
                    Update update = new Update(true);
                    update.setVisible(true);
                }
        }
    };
    ActionListener upInfListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("上传作者")) {
                upAuthor up = new upAuthor();
                up.setVisible(true);
            }
        }
    };
    ActionListener cAL = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("更新图书")) {
                Update update = new Update(false);
                update.setVisible(true);
            }
        }
    };
    ActionListener del = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getActionCommand().equals("删除图书")) {
                delUser del = new delUser(false);
                del.setVisible(true);
            }
        }
    };
    public managePage(){
        setTitle("管理员中心");
        setSize(800,500);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setMenu();
        setPage();
    }
    public void setMenu(){
        bar = new JMenuBar();
        powerful = new JMenu("权限");
        delData = new JMenuItem("删除记录");
        delData.addActionListener(delDataAct);
        delUser = new JMenuItem("删除用户");
        delUser.addActionListener(delUserAct);
        showData = new JMenuItem("查看归还记录");
        showData.addActionListener(showAct);
        transport=new JMenuItem("上传资源");
        transport.addActionListener(updateListener);
        upInf = new JMenuItem("上传作者");
        upInf.addActionListener(upInfListener);
        change = new JMenuItem("更新图书");
        change.addActionListener(cAL);
        delBook = new JMenuItem("删除图书");
        delBook.addActionListener(del);
        bar.add(powerful);
        powerful.add(delData);
        powerful.add(delUser);
        powerful.add(showData);
        powerful.add(transport);
        powerful.add(upInf);
        powerful.add(change);
        powerful.add(delBook);
        setJMenuBar(bar);
    }
    public void setPage(){
        searchField = new JTextField(50);
        JButton searchButton = new JButton("搜索");
        searchButton.addActionListener(searchRecord);
        JPanel panel1=new JPanel();
        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout(FlowLayout.LEFT,40,30));
        area = new JTextArea(20,110);
        Font font = new Font("宋体",Font.PLAIN,20);
        area.setFont(font);
        panel2.add(area);
        panel1.add(searchField);
        panel1.add(searchButton);
        add(panel1,BorderLayout.NORTH);
        add(panel2,BorderLayout.CENTER);
    }
    public void printRecord(int uId,boolean sign){
        try{
            // 注册 JDBC 驱动
            Class.forName(JDBC_DRIVER);
            // 打开链接
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            // 执行查询
            String sql = null;
            area.setText("");
            if(sign) {
                area.append("\t借阅时间\t\t\t借阅书籍" + "\n");
                sql = "select  browTime,browBook from  browrecord where browId = ?";
            }
            else {
                area.append("\t归还时间\t\t\t归还书籍" + "\n");
                sql = "select  returnTime,returnBook from  browreturn where returnId = ?";
                choice = 0 ;
            }
            stmt= conn.prepareStatement(sql);
            stmt.setInt(1, uId);
            rs = stmt.executeQuery();
            // 展开结果集数据库
            while(rs.next()){
                if(sign)
                    area.append("\t" + rs.getDate("browTime") + "\t\t" + rs.getString("browBook") + "\n");
                else
                    area.append("\t" + rs.getDate("returnTime") + "\t\t" + rs.getString("returnBook") + "\n");
            }
            // 完成后关闭
            rs.close();
            stmt.close();
            conn.close();
        }catch(SQLException se){
            // 处理 JDBC 错误
            se.printStackTrace();
        }catch(Exception e){
            // 处理 Class.forName 错误
            e.printStackTrace();
        }finally{
            // 关闭资源
            try{
                if(stmt!=null) stmt.close();
            }catch(SQLException se2){
            }// 什么都不做
            try{
                if(conn!=null) conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }
        }
    }
}

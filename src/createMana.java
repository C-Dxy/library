import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class createMana extends JFrame {
    JPanel panel;
    JLabel idLabel,accountLabel,usernameLabel;
    JTextField idField,accountField,usernameField;
    JPasswordField passwordField,passwordField2;
    JButton registerButton;
    public createMana(){
        setTitle("创建管理员");
        setSize(300, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        panel = new JPanel();
        panel.setLayout(new GridLayout(6, 3));
        idLabel = new JLabel("管理员ID：");
        idField = new JTextField();
        accountLabel = new JLabel("管理员账号：");
        accountField = new JTextField();
        usernameLabel = new JLabel("用户名：");
        usernameField = new JTextField();
        JLabel passwordLabel = new JLabel("密码：");
        passwordField = new JPasswordField();
        JLabel password = new JLabel("确认密码：");
        passwordField2 = new JPasswordField();
        registerButton = new JButton("注册");
        registerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 处理注册的逻辑，可以获取输入的用户名和密码进行注册操作
                int id = Integer.parseInt(idField.getText());
                String account = accountField.getText();
                String username = usernameField.getText();
                String password = new String(passwordField.getPassword());
                String password2 = new String(passwordField2.getPassword());
                if(account.isEmpty() || username.isEmpty() || id == 0){
                    System.out.println("管理员ID或用户名或手机号不能为空！");
                }
                else if(password2.equals(password)) {
                    new dataRoom().insertMana(id,account, username,password);
                    dispose();
                }
                else System.out.println("两次密码不一致，请重新输入！");
            }
        });
        panel.add(idLabel);
        panel.add(idField);
        panel.add(accountLabel);
        panel.add(accountField);
        panel.add(usernameLabel);
        panel.add(usernameField);
        panel.add(passwordLabel);
        panel.add(passwordField);
        panel.add(password);
        panel.add(passwordField2);
        panel.add(registerButton);
        add(panel, BorderLayout.CENTER);
    }
}
